import git
import os
import subprocess
import yaml
import hashlib
import warnings
from utils import typeEnterToContinue
from dockerModule import getWorkir, buildDockerImage, runDockerImage

params = None

def init(repository,branch) -> None :
    folder = repository.split('/')[-1].split('.')[0]
    if not os.path.exists(folder) :
        git.Git("./").clone(repository)
    try : 
        git.Repo(folder).git.checkout(branch)
    except git.exc.GitCommandError : 
        raise Exception(f"Branch {branch} not found in the repository")
    os.chdir(folder)

def getParameters() -> None :
    global params
    if not (os.path.exists('experimentResume.yaml')):
        raise Exception("No exeperimentResume.yaml file found, the branch is not an exeperiment")
    with open('experimentResume.yaml', 'r') as stream:
        params = yaml.safe_load(stream)

def runExperiment() -> None :
    file = open(params.get('commands'), "r")
    for line in file.read().splitlines():
        print(f"running {line} ...")
        process = subprocess.run(line,shell=True)
        process.check_returncode()
        print("done")

def checkForInstructions() -> None :
    if (params.get('instruction') != None) :
        print("You can check the instructions for the experiment in the file : " + params.get('instruction'))
    else :
        warnings.warn("No instructions for the experiment found in the repository")
    typeEnterToContinue("Run the exepriment and then press enter when it's done")

def genChecksum(file) -> str :
    hash_md5 = hashlib.md5()
    with open(file, "rb") as f:
        for chunk in iter(lambda: f.read(4096), b""):
            hash_md5.update(chunk)
    return hash_md5.hexdigest()

def genChecksums() -> list[dict]:
    checksums = []
    outputFolder = params.get('outputFolder')
    for file in os.listdir(outputFolder) :
        if not file.endswith(".gitkeep"):
            checksums.append({f"{outputFolder}/{file}" : genChecksum(f'{outputFolder}/{file}')})
    return checksums


def compareChecksums() -> bool:
    changes = False
    for (dict1, dict2) in zip(params.get('checksums'), genChecksums()):
        for (key, value) in dict1.items():
            if dict2.get(key) != value :
                warnings.warn(f"{key} has changed")
                changes = True
    return changes

def run(repository, branch) -> None :
    print("Initializing the experiment repository ...")
    init(repository, branch)
    print("Getting the experiment parameters ...")
    getParameters()
    print("Running the experiment ...")
    if (params.get('dockerfile')) :
        print("Dockerimage was found ! Using it to run the experiment...")
        name = params.get("name")
        buildDockerImage(name)
        runDockerImage(name,getWorkir("Dockerfile"))
    else:
        if (params.get('commands') != None) : 
            runExperiment()
        else :
            checkForInstructions()
    print("Comparing checksums of the outputs ...")
    if (compareChecksums()) : 
        print("The exepriment was reproduced with succes but some output files are differents.")
    else :
        print("The exepriment was reproduced with succes !")