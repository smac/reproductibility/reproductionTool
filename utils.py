
def yesOrNo(question) -> bool:
    while True:
        answer = input(question + " (y/n): ")
        if answer == 'y' or answer == 'n':
            return answer == 'y'

def typeEnterToContinue(text) -> None:
    answer = "not empty"
    while (answer != "") :
        answer = input(text)