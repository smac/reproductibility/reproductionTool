import subprocess

def getWorkir(dockerFile) -> str :
    workdir = "/" 
    with open(dockerFile,"r") as file:
        for line in file.read().splitlines():
            if line.startswith("WORKDIR"):
                workdir = line.split(" ")[1]
    return workdir

def buildDockerImage(imageName) -> None:
    print("Building the docker image ...")
    try :
        subprocess.run(f"docker build -t {imageName.lower()}experiment ./",shell=True).check_returncode()
    except :
        subprocess.run(f"sudo docker build -t {imageName.lower()}experiment ./",shell=True).check_returncode()

def runDockerImage(imageName,workDirectory) -> None:
    print("binding docker image to the current directory and running it...")
    try:
        subprocess.run(f"docker run -it --mount type=bind,source=\"$PWD\",target={workDirectory} {imageName.lower()}experiment",shell=True).check_returncode()
    except :
        subprocess.run(f"sudo docker run -it --mount type=bind,source=\"$PWD\",target={workDirectory} {imageName.lower()}experiment",shell=True).check_returncode()